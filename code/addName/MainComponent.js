import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { addName } from "./reducer";

function MainComponent(props) {
  const {list, addName} = props;
  let input;

  const clickHandler = (e) => {
    let name = input.value;
    if(name !== ''){
      input.value = '';
      addName(name);
    }else{
      alert('please enter a name');
    }
  };

  return (
    <>
      <div className='container'>
        <div className='card'>
          <input ref={node =>{ input = node}} 
          type='text' placeholder='Enter a Name..' className='header' />
          <button onClick={clickHandler}>Add</button>
          <div className='list'>
            <p>List of the Names added</p>
            <ul>
              {
                list.map(item => {
                  let id = item.id;
                  return (
                    <li 
                      key={id}
                    >
                      {item.text}
                    </li>
                  )
                })
              }
            </ul>
          </div>
        </div>
      </div>
      <style jsx='true'>{`
      `}</style>
    </>
  );
  
}

const mapDispatchtoProps = dispatch => ({
  addName: bindActionCreators(addName, dispatch)
})

const mapStatetoProps = state => ({
  list: state
});

export default connect(mapStatetoProps, mapDispatchtoProps)(MainComponent);