import React from "react";

function MainComponent(props) {
  
  let input;

  const clickHandler = (e) => {
    let name = input.value;
    if(name !== ''){
      input.value = '';
      alert(name);
    }else{
      alert('please enter a name');
    }
  };

  return (
    <>
      <div className='container'>
        <div className='card'>
          <input ref={node =>{ input = node}} 
          type='text' placeholder='Enter a Name..' className='header' />
          <button onClick={clickHandler}>Add</button>
          <div className='list'>
            <p>List of the Names added</p>
            <ul>
              <li >
                Advait
              </li>
            </ul>
          </div>
        </div>
      </div>
      <style jsx='true'>{`
      `}</style>
    </>
  );
  
}

export default MainComponent;