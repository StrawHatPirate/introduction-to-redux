import React from "react";
import { connect, useDispatch,useStore } from "react-redux";
import { bindActionCreators } from "redux";
import { addName, REMOVE_NAME } from "./reducer";

function MainComponent(props) {
  const {list, addName} = props;
  const dispatch = useDispatch();
  const store = useStore();

  console.log(store.getState());
  
  let input;

  const clickHandler = (e) => {
    let name = input.value;
    if(name !== ''){
      input.value = '';
      addName(name);
    }else{
      alert('please enter a name');
    }
  };

  return (
    <>
      <div className='container'>
        <div className='card'>
          <input ref={node =>{ input = node}} 
          type='text' placeholder='Enter a Name..' className='header' />
          <button onClick={clickHandler}>Add</button>
          <div className='list'>
            <p>List of the Names added</p>
            <ul>
              {
                list.map(item => {
                  let id = item.id;
                  return (
                    <li 
                      key={id}
                      onClick={(e)=> { dispatch({type: REMOVE_NAME, id}) } }
                    >
                      {item.text}
                    </li>
                  )
                })
              }
              
            </ul>
          </div>
        </div>
      </div>
      <style jsx='true'>{`
      `}</style>
    </>
  );
  
}

const mapDispatchtoProps = dispatch => ({
  addName: bindActionCreators(addName, dispatch)
})

const mapStatetoProps = state => ({
  list: state
});

export default connect(mapStatetoProps, mapDispatchtoProps)(MainComponent);